#%%

import netgen.gui
## im browser/jupyter notebook:
##%gui tk

from netgen.geom2d import SplineGeometry

from ngsolve import *

#%%

geo = SplineGeometry()

geo.AppendPoint(0,0)
geo.AppendPoint(48,44)
geo.AppendPoint(48,44+8)
geo.AppendPoint(48,44+16)
geo.AppendPoint(0,44)

## "line" -> gerades segment
## 0, 1 -> von 0 nach 1
## bc -> boundary condition
geo.Append(['line',0,1],leftdomain=1,rightdomain=0,bc="free")
geo.Append(['line',1,2],leftdomain=1,rightdomain=0,bc="force")
geo.Append(['line',2,3],leftdomain=1,rightdomain=0,bc="force")
geo.Append(['line',3,4],leftdomain=1,rightdomain=0,bc="free")
geo.Append(['line',4,0],leftdomain=1,rightdomain=0,bc="fix")

geo.Draw()
# %%

## generate the mesh
mesh = Mesh(geo.GenerateMesh(maxh=10))

## refine (1 triangle -> 4 triangles)
mesh.Refine()
mesh.Refine()
mesh.Refine()

## draw the mesh in gui
Draw(mesh)

#%%

## finite element space - linear shape function (hat basis functions, cmp. Numerik und Optimierung)
## dirichlet -> u=0 boundary condition
V = VectorH1(mesh, order=3, dirichlet="fix")

# generate a finite element function with degree of freedom vector
## available as u.vec
u = GridFunction(V)

## plot the displacement u in gui
Draw(u, mesh, "u")
# %%
Emodul = 2.4
nu = 0.49

## plane strain
lam = Emodul*nu/(1+nu)/(1-2*nu)
mu = Emodul/2/(1+nu)


# ## plane stress
# lam = Emodul*nu/(1-nu*nu)
# mu = Emodul/2/(1+nu)


Force = 1/16

## symbolic variables 
U = V.TrialFunction()
DELTAU = V.TestFunction()

## derived symbolic quantities:
## displacement gradient
GRADU = Grad(U)
## linearized strain tensor
EPS = 0.5*(GRADU + GRADU.trans)
## virtual displacement gradient
DELTAGRADU = Grad(DELTAU)
## virtual strain
DELTAEPS = 0.5*(DELTAGRADU + DELTAGRADU.trans)

## Hooke's law
SIGMA = 2*mu*EPS + lam*Trace(EPS)*Id(2)

## bilinear form - container for principle of virtual work integrals
a = BilinearForm(V)
## virtual work of internal forces
# a += SymbolicBFI(Emodul*InnerProduct(EPS,DELTAEPS))
a += SymbolicBFI(InnerProduct(SIGMA, DELTAEPS))
## virtual work of surface load
a += SymbolicBFI(-Force*DELTAU[1], definedon=mesh.Boundaries("force"))

#%%

## assembling the stiffness matrix, available as a.mat
a.AssembleLinearization(u.vec)

## create a right hand side vector
res = u.vec.CreateVector()

u.vec[:] = 0

## compute the right hand side vector
a.Apply(u.vec, res)
res.data *= -1

## compute the inverse of the stiffness matrix
inva = a.mat.Inverse(V.FreeDofs())

## solve the problem
u.vec.data = inva * res







# %%

eps = 0.5*(grad(u) + grad(u).trans)
# sigma = Emodul * eps
sigma = 2*mu*eps + lam*Trace(eps)*Id(2)

Draw(eps, mesh, "eps")
Draw(sigma, mesh, "sigma")

# %%

## evaluate at P
uP = u(mesh(48,52))

print("u(P) = ", uP)
# %%

uMean = 1/16*Integrate(u, mesh, definedon=mesh.Boundaries("force"))

print("uMean = ", uMean)
# %%

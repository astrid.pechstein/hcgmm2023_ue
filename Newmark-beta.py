#%%
import netgen.gui

from netgen.geom2d import SplineGeometry
from ngsolve import *

import numpy as np
#%%
thickness = 1e-3
length = 10e-3

geo = SplineGeometry()
pnts = [ (0,-thickness/2), (length,-thickness/2), (length,thickness/2), (0,thickness/2) ]
pind = [ geo.AppendPoint(*pnt) for pnt in pnts ]
geo.Append(['line',pind[0],pind[1]],leftdomain=1,rightdomain=0,bc="bottom")
geo.Append(['line',pind[1],pind[2]],leftdomain=1,rightdomain=0,bc="right")
geo.Append(['line',pind[2],pind[3]],leftdomain=1,rightdomain=0,bc="top")
geo.Append(['line',pind[3],pind[0]],leftdomain=1,rightdomain=0,bc="left",maxh=thickness/5)
mesh = Mesh(geo.GenerateMesh(maxh=thickness/3))
Draw(mesh)

#%%

E = 2e11
nu = 0.3
rho = 7.8e3

C = E/(1-nu*nu)*CoefficientFunction((1, nu, 0,    nu, 1, 0,   0, 0, (1-nu)/2), dims=(3,3))
S = 1/E*CoefficientFunction((1, -nu, 0,   -nu, 1, 0,   0, 0, 2*(1+nu)), dims=(3,3))

f = 5e6

def strain(u): 
    return CoefficientFunction((u.Deriv()[0,0], u.Deriv()[1,1], u.Deriv()[0,1]+u.Deriv()[1,0]))


#%%

## static problem
Vu = VectorH1(mesh, order=1, dirichlet="left")

u_static = GridFunction(Vu)
Draw(u_static, mesh, "u_static")

U = Vu.TrialFunction()
deltaU = Vu.TestFunction()

a_static = BilinearForm(Vu)
a_static += SymbolicBFI(InnerProduct(strain(U),C*strain(deltaU)).Compile() )
a_static += SymbolicBFI((f * deltaU[1]).Compile(), definedon=mesh.Boundaries("right") )

res = u_static.vec.CreateVector()
a_static.AssembleLinearization(u_static.vec)
a_static.Apply(u_static.vec, res)

u_static.vec.data -= a_static.mat.Inverse(Vu.FreeDofs(), inverse="sparsecholesky") * res

w_s = u_static(mesh(length,0))[1]
print("static deflection", w_s)

#%%

u = GridFunction(Vu)
v = GridFunction(Vu)

u_n = GridFunction(Vu)
v_n = GridFunction(Vu)

delta_u = GridFunction(Vu)
delta_v = GridFunction(Vu)

m = BilinearForm(Vu)
m += SymbolicBFI(rho*InnerProduct(U,deltaU))
m.Assemble()

k = BilinearForm(Vu)
k += SymbolicBFI((InnerProduct(strain(U),C*strain(deltaU)).Compile()))
k.Assemble()

fvec = LinearForm(Vu)
fvec += SymbolicLFI(-f*deltaU[1],definedon=mesh.Boundaries('right'))
fvec.Assemble()

gamma = 0.5
beta = 0.5*gamma

timestepsize = 5e-6

helpmatrix = m.mat.CreateMatrix()
helpmatrix.AsVector().data = (m.mat.AsVector() + beta*(timestepsize**2)*k.mat.AsVector())

invhelpmatrix = helpmatrix.Inverse(Vu.FreeDofs())

rhs = u.vec.CreateVector()
#%%
t_0 = 0
t_n = 0
t_n1 = 0
omega = np.pi*18*1e3

f_n = u.vec.CreateVector()
f_n1 = u.vec.CreateVector()

w_values = []
time = []

for step in range(0,100):
    u_n.vec.data = u.vec
    v_n.vec.data = v.vec

    t_n = timestepsize*step
    t_n1 = t_n + timestepsize
    f_n.data = sin(omega*t_n)*fvec.vec
    f_n1.data = sin(omega*t_n1)*fvec.vec

    rhs.data = (timestepsize*(m.mat*v_n.vec)-0.5*timestepsize**2*(k.mat*u_n.vec-(1-2*beta)*f_n-2*beta*f_n1))
    delta_u.vec.data = invhelpmatrix*rhs

    delta_v.vec.data = 2*timestepsize*(delta_u.vec-timestepsize*v_n.vec)
    
    u.vec.data = u_n.vec + delta_u.vec
    v.vec.data = v_n.vec + delta_v.vec

    w_values.append(u(mesh(length,0))[1])
    time.append(t_n1)
    
Draw(u,mesh,"u")
Draw(v,mesh,"v")

import matplotlib.pyplot as plt
plt.plot(time, w_values)
#%%

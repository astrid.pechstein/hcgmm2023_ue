#%%

import netgen.gui
## im browser/jupyter notebook:
##%gui tk

from netgen.geom2d import SplineGeometry

from ngsolve import *

#%%

geo = SplineGeometry()

geo.AppendPoint(0,0)
geo.AppendPoint(48,44)
geo.AppendPoint(48,44+8)
geo.AppendPoint(48,44+16)
geo.AppendPoint(0,44)

## "line" -> gerades segment
## 0, 1 -> von 0 nach 1
## bc -> boundary condition
geo.Append(['line',0,1],leftdomain=1,rightdomain=0,bc="free")
geo.Append(['line',1,2],leftdomain=1,rightdomain=0,bc="force")
geo.Append(['line',2,3],leftdomain=1,rightdomain=0,bc="force")
geo.Append(['line',3,4],leftdomain=1,rightdomain=0,bc="free")
geo.Append(['line',4,0],leftdomain=1,rightdomain=0,bc="fix")

geo.Draw()
# %%

## generate the mesh
mesh = Mesh(geo.GenerateMesh(maxh=10))

## refine (1 triangle -> 4 triangles)
mesh.Refine()
mesh.Refine()
# mesh.Refine()

## draw the mesh in gui
Draw(mesh)

#%%

## finite element space - linear shape function (hat basis functions, cmp. Numerik und Optimierung)
## dirichlet -> u=0 boundary condition
V = VectorH1(mesh, order=3, dirichlet="fix")

# generate a finite element function with degree of freedom vector
## available as u.vec
u = GridFunction(V)

## plot the displacement u in gui
Draw(u, mesh, "u")
# %%
Emodul = 2.4
nu = 0.49

## plane strain
lam = Emodul*nu/(1+nu)/(1-2*nu)
mu = Emodul/2/(1+nu)

Force = 1/16

## symbolic variables 
U = V.TrialFunction()
DELTAU = V.TestFunction()

## derived symbolic quantities:
## displacement gradient
GRADU = Grad(U)
## virtual displacement gradient
DELTAGRADU = Grad(DELTAU)

F = Id(2) + GRADU
DELTAF = DELTAGRADU
E = 0.5*(F.trans*F - Id(2))
DELTAE = 0.5*(DELTAF.trans*F + F.trans*DELTAF)

S = 2*mu*E + lam*Trace(E)*Id(2)


## bilinear form - container for principle of virtual work integrals
a = BilinearForm(V)
## virtual work of internal forces
# a += SymbolicBFI(Emodul*InnerProduct(EPS,DELTAEPS))
a += SymbolicBFI(InnerProduct(S, DELTAE))
## virtual work of surface load
a += SymbolicBFI(-Force*DELTAU[1], definedon=mesh.Boundaries("force"))

#%%

u.vec[:] = 0

solvers.Newton(a, u, maxit=10, inverse="sparsecholesky")

# alternatives:
# "pardiso" -- usually available on windows, great performance/stability
# "umfpack" -- usually available on windows?, macos, good performance/stability
# "sparsecholesky" -- always available, only for symmetric positive definite problems (no Lagrange multpliers, no pressure elements,...)







# %%

strain = 0.5*((Id(2)+grad(u)).trans*(Id(2) + grad(u))-Id(2))
# sigma = Emodul * eps
stress = 2*mu*strain + lam*Trace(strain)*Id(2)

Draw(strain, mesh, "strain")
Draw(stress, mesh, "stress")

# %%

## evaluate at P
uP = u(mesh(48,52))

print("u(P) = ", uP)
# %%

uMean = 1/16*Integrate(u, mesh, definedon=mesh.Boundaries("force"))

print("uMean = ", uMean)
# %%

#%%
import netgen.gui

from netgen.geom2d import SplineGeometry
from ngsolve import *

import numpy as np
#%%
thickness = 1e-3
length = 10e-3

geo = SplineGeometry()
pnts = [ (0,-thickness/2), (length,-thickness/2), (length,thickness/2), (0,thickness/2) ]
pind = [ geo.AppendPoint(*pnt) for pnt in pnts ]
geo.Append(['line',pind[0],pind[1]],leftdomain=1,rightdomain=0,bc="bottom")
geo.Append(['line',pind[1],pind[2]],leftdomain=1,rightdomain=0,bc="right")
geo.Append(['line',pind[2],pind[3]],leftdomain=1,rightdomain=0,bc="top")
geo.Append(['line',pind[3],pind[0]],leftdomain=1,rightdomain=0,bc="left",maxh=thickness/5)
mesh = Mesh(geo.GenerateMesh(maxh=thickness/3))
Draw(mesh)

#%%

E = 2e11
nu = 0.3
rho = 7.8e3

C = E/(1-nu*nu)*CoefficientFunction((1, nu, 0,    nu, 1, 0,   0, 0, (1-nu)/2), dims=(3,3))
S = 1/E*CoefficientFunction((1, -nu, 0,   -nu, 1, 0,   0, 0, 2*(1+nu)), dims=(3,3))

f = 5e6

def strain(u): 
    return CoefficientFunction((u.Deriv()[0,0], u.Deriv()[1,1], u.Deriv()[0,1]+u.Deriv()[1,0]))


#%%

## static problem
Vu = VectorH1(mesh, order=1, dirichlet="left")

u_static = GridFunction(Vu)
Draw(u_static, mesh, "u_static")

U = Vu.TrialFunction()
deltaU = Vu.TestFunction()

a_static = BilinearForm(Vu)
a_static += SymbolicBFI(InnerProduct(strain(U),C*strain(deltaU)).Compile() )
a_static += SymbolicBFI((f * deltaU[1]).Compile(), definedon=mesh.Boundaries("right") )

res = u_static.vec.CreateVector()
a_static.AssembleLinearization(u_static.vec)
a_static.Apply(u_static.vec, res)

u_static.vec.data -= a_static.mat.Inverse(Vu.FreeDofs(), inverse="sparsecholesky") * res

w_s = u_static(mesh(length,0))[1]
print("static deflection", w_s)

#%%

## time-dependent problem - explicit Euler

u = GridFunction(Vu)
v = GridFunction(Vu)

u_n = GridFunction(Vu)
v_n = GridFunction(Vu)

delta_u = GridFunction(Vu)
delta_v = GridFunction(Vu)

timesteps = np.linspace(0,10e-5,2000*1)
w_values = []

deltaT = Parameter(0)

m = BilinearForm(Vu)
m += SymbolicBFI(rho*InnerProduct(U,deltaU))
m.Assemble()
minv = m.mat.Inverse(Vu.FreeDofs(),"sparsecholesky")

rhs = LinearForm(Vu)
rhs += SymbolicLFI(-deltaT*InnerProduct(strain(u_n),C*strain(deltaU)) )
rhs += SymbolicLFI(-deltaT*f*deltaU[1], definedon=mesh.Boundaries("right"))

t_n = 0

for t in timesteps:
    deltaT.Set(t - t_n)
    u_n.vec.data = u.vec
    v_n.vec.data = v.vec

    rhs.Assemble()

    delta_v.vec.data = minv*rhs.vec

    v.vec.data += delta_v.vec

    u.vec.data = u_n.vec + deltaT.Get()*v.vec

    w_values.append(u(mesh(length,0))[1])

    t_n = t

import matplotlib.pyplot as plt
plt.plot(timesteps, w_values)

#%%


## time-dependent problem - implicit Euler

u = GridFunction(Vu)
v = GridFunction(Vu)

u_n = GridFunction(Vu)
v_n = GridFunction(Vu)

delta_u = GridFunction(Vu)
delta_v = GridFunction(Vu)

timesteps = np.linspace(0,5e-4,100)
w_values = []

deltaT = Parameter(0)

DeltaV, deltaU = Vu.TnT()
a = BilinearForm(Vu)
a += SymbolicBFI(rho*InnerProduct(DeltaV,deltaU))
a += SymbolicBFI(deltaT*InnerProduct(strain(u_n) + deltaT*strain(v_n)+ deltaT*strain(DeltaV),C*strain(deltaU)) )
a += SymbolicBFI(deltaT*f*deltaU[1], definedon=mesh.Boundaries("right"))



t_n = 0
for t in timesteps:
    deltaT.Set(t - t_n)
    u_n.vec.data = u.vec
    v_n.vec.data = v.vec

    a.Apply(delta_v.vec, res)

    ## can be precomputed if time step is constant
    a.AssembleLinearization(delta_v.vec)
    ainv = a.mat.Inverse(Vu.FreeDofs(), inverse = "sparsecholesky")
    ## end precomputed

    delta_v.vec.data -= ainv*res

    v.vec.data += delta_v.vec

    u.vec.data = u_n.vec + deltaT.Get()*v.vec

    w_values.append(u(mesh(length,0))[1])

    t_n = t

import matplotlib.pyplot as plt
plt.plot(timesteps, w_values)


#%%


## time-dependent problem - trapezoidal rule

u = GridFunction(Vu)
v = GridFunction(Vu)

u_n = GridFunction(Vu)
v_n = GridFunction(Vu)

delta_u = GridFunction(Vu)
delta_v = GridFunction(Vu)

timesteps = np.linspace(0,5e-4,100)
w_values = []

deltaT = Parameter(0)

DeltaV, deltaU = Vu.TnT()
a = BilinearForm(Vu)
a += SymbolicBFI(rho*InnerProduct(DeltaV,deltaU))
a += SymbolicBFI(deltaT*InnerProduct(strain(u_n) + 0.5*deltaT*strain(v_n)+ 0.5*deltaT*strain(DeltaV),C*strain(deltaU)) )
a += SymbolicBFI(deltaT*f*deltaU[1], definedon=mesh.Boundaries("right"))



t_n = 0
for t in timesteps:
    deltaT.Set(t - t_n)
    u_n.vec.data = u.vec
    v_n.vec.data = v.vec

    a.Apply(delta_v.vec, res)

    ## can be precomputed if time step is constant
    a.AssembleLinearization(delta_v.vec)
    ainv = a.mat.Inverse(Vu.FreeDofs(), inverse = "sparsecholesky")
    ## end precomputed

    delta_v.vec.data -= ainv*res

    v.vec.data += delta_v.vec

    u.vec.data = u_n.vec + deltaT.Get()*(v_n.vec + 0.5*delta_v.vec)

    w_values.append(u(mesh(length,0))[1])

    t_n = t

import matplotlib.pyplot as plt
plt.plot(timesteps, w_values)

#%%


## time-dependent problem - Newmark beta scheme

u = GridFunction(Vu)
v = GridFunction(Vu)

u_n = GridFunction(Vu)
v_n = GridFunction(Vu)

delta_u = GridFunction(Vu)
delta_v = GridFunction(Vu)

timesteps = np.linspace(0,5e-4,100)
w_values = []

deltaT = Parameter(0)

gamma = 0.5
beta = 0.5*gamma

DU, deltaU = Vu.TnT()
a = BilinearForm(Vu)
a += SymbolicBFI(rho*InnerProduct(DU-deltaT*v_n,deltaU))
a += SymbolicBFI(0.5*deltaT**2*InnerProduct(strain(u_n) + 2*beta*strain(DU),C*strain(deltaU)) )
a += SymbolicBFI(0.5*deltaT**2*f*deltaU[1], definedon=mesh.Boundaries("right"))

m = BilinearForm(Vu)
m += SymbolicBFI(rho*InnerProduct(DU,deltaU))
m.Assemble()
minv = m.mat.Inverse(Vu.FreeDofs(), inverse="sparsecholesky")

rhs = LinearForm(Vu)
rhs += SymbolicLFI(deltaT*(1-gamma)*InnerProduct(-strain(u_n),C*strain(deltaU)))
rhs += SymbolicLFI(deltaT*(gamma)*InnerProduct(-strain(u),C*strain(deltaU)))
rhs += SymbolicLFI(-deltaT*f*deltaU[1], definedon=mesh.Boundaries("right"))

t_n = 0
for t in timesteps:
    deltaT.Set(t - t_n)
    u_n.vec.data = u.vec
    v_n.vec.data = v.vec

    a.Apply(delta_u.vec, res)

    ## can be precomputed if time step is constant
    a.AssembleLinearization(delta_u.vec)
    ainv = a.mat.Inverse(Vu.FreeDofs(), inverse = "sparsecholesky")
    ## end precomputed

    delta_u.vec.data -= ainv*res

    u.vec.data += delta_u.vec

    rhs.Assemble()
    delta_v.vec.data = minv*rhs.vec

    v.vec.data += delta_v.vec

    w_values.append(u(mesh(length,0))[1])

    t_n = t

import matplotlib.pyplot as plt
plt.plot(timesteps, w_values)

#%%